// Теоретический ответ. Метод forEach работает аналогично циклу for, перебирая каждый элемент массива.

const arry = ['hello', 'world', 23, '23', null];

function filterBy(arry, type) {

	const selectedArry = arry.filter((element) => { // фильтруем все элем. массива, возвращаятолько те элементы,
		return !type.includes(typeof element); // которые НЕ соответствуют типу type		
	});

	return selectedArry;
}

console.log(filterBy(arry, 'string'));
console.log(filterBy(arry, 'number'));
console.log(filterBy(arry, 'object'));